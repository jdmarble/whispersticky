# Whisper Sticky

A proof of concept for exposing a [Jexer](https://jexer.sourceforge.io/) application
over WebSockets to a browser using [xterm.js](https://xtermjs.org/).
