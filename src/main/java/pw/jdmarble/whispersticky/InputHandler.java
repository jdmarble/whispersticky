package pw.jdmarble.whispersticky;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import jexer.TApplication;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

public class InputHandler extends BinaryWebSocketHandler {

    /**
     * The web socket writes to these which gets piped to the input of the Jexer application.
     */
    private final Map<String, OutputStream> webSocketPipes = Collections.synchronizedMap(new HashMap<>());

    @Override
    public void afterConnectionEstablished(final WebSocketSession session) throws Exception {

        // Handle data coming from the web socket and going to the application.
        final PipedInputStream applicationInputPipe = new PipedInputStream();
        final PipedOutputStream websocketPipe = new PipedOutputStream(applicationInputPipe);
        webSocketPipes.put(session.getId(), websocketPipe);

        // Handle data coming from the application and going to the web socket.
        final OutputStream applicationOutput = new OutputStream() {
            @Override
            public void write(final int i) throws IOException {
                session.sendMessage(new BinaryMessage(new byte[(byte) i]));
            }

            @Override
            public void write(final byte[] b) throws IOException {
                session.sendMessage(new BinaryMessage(b));
            }

            @Override
            public void write(final byte[] b, final int off, final int len) throws IOException {
                session.sendMessage(new BinaryMessage(b, off, len, true));
            }
        };

        final TApplication app = new TApplication(applicationInputPipe, applicationOutput);
        app.addToolMenu();
        app.addFileMenu();
        app.addWindowMenu();
        // TODO: Don't do this!
        new Thread(app).start();
    }

    @Override
    protected void handleBinaryMessage(final WebSocketSession session, final BinaryMessage message) throws Exception {
        webSocketPipes.get(session.getId()).write(message.getPayload().array());
    }

    @Override
    protected void handleTextMessage(final WebSocketSession session, final TextMessage message) {
        try {
            webSocketPipes.get(session.getId()).write(message.getPayload().getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
