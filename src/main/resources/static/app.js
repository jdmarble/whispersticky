
$(function () {
    const socket = new WebSocket('ws:/localhost:8080/gs-guide-websocket');
    socket.onopen = function() {
        const term = new Terminal();
        const attachAddon = new AttachAddon.AttachAddon(socket);
        term.loadAddon(attachAddon);
        term.open(document.getElementById('terminal'));
    };
});

